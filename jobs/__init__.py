import logging
from typing import Callable

from jobs.exceptions import StateIntegriyError
from jobs.states import JobState, ResponseJobState, BlueprintJobState


class BaseJobWrapper:
    """
    This is a simple wrapper which will execute jobs, passed when initialized this wrapper.
    Every wrapper has its own jobstate_class, it means that inside of this wrapper all
    jobs should return the same jobstate_class. If integrity of jobstate will be violated while
    executing one of the jobs - previous successful state will be returned

    Initial jobstate can also be provided as an argument while initialization.
    """

    jobstate_class: type(JobState) = JobState

    def __init__(self, *jobs: Callable, initial_jobstate=None):
        from config import DEFAULT_REQUEST_BUILDER

        self.errors = dict()
        self.initial_jobstate = initial_jobstate or self.jobstate_class()
        if not isinstance(self.initial_jobstate, JobState):
            self.initial_jobstate = JobState()

        self.request_builder_class = DEFAULT_REQUEST_BUILDER
        self.jobs = jobs

    def __call__(self, *args, jobstate=None, **kwargs) -> JobState:
        jobstate = jobstate or self.initial_jobstate
        if jobstate.__class__ != self.initial_jobstate.__class__:
            logging.error('Cannot match initial state %s with %s (%s)' %
                          (self.initial_jobstate, jobstate, self.__class__.__name__))
            jobstate = self.initial_jobstate

        for job in self.jobs:
            try:
                new_jobstate = job(jobstate)
                if not self._check_jobstate_integrity(new_jobstate):
                    raise StateIntegriyError('Cannot match initial state %s with state, returned '
                                             'from %s. Terminating...' %
                                             (self.initial_jobstate, getattr(job, '__name__', None)))
            except StateIntegriyError as exc:
                logging.warning(exc)
                break
            except Exception as exc:
                jobstate.error(job, exc)
                for key, value in jobstate.get_errors():
                    self.errors[key] = value
            else:
                jobstate = new_jobstate
                continue

        self._handle_errors()
        return jobstate

    def _check_jobstate_integrity(self, jobstate) -> bool:
        if isinstance(jobstate, list):
            return all(map(lambda x: self._check_jobstate_integrity(x), jobstate))
        else:
            return jobstate.__class__ == self.initial_jobstate.__class__ \
                   or isinstance(jobstate, self.initial_jobstate.__class__)

    def _handle_errors(self):
        if self.errors:
            logging.error({
                'message': 'There are exceptions while executing jobs in %s' % self.__class__.__name__,
                'errors': self.errors
            })


class RequestJobWrapper(BaseJobWrapper):
    """
    After all jobs will be executed, wrapper will prepare a request with
    the help of request_builder_class and return a ResponseJobState as a
    result of his __call__ method
    """

    def __init__(self, *jobs: Callable, initial_jobstate=None):
        if getattr(self, 'request_builder_class', None) is None:
            from config import DEFAULT_REQUEST_BUILDER
            self.request_builder_class = DEFAULT_REQUEST_BUILDER

        super(RequestJobWrapper, self).__init__(*jobs, initial_jobstate=initial_jobstate)

    def __call__(self, jobstate=None, *args, **kwargs) -> ResponseJobState:
        """
        If request_builder_class is provided then result of this request (Response),
        wrapped to the RequestJobState object will be returned.
        Otherwise DEFAULT_REQUEST_BUILDER will be used.
        """
        jobstate = super(RequestJobWrapper, self).__call__(jobstate=jobstate, *args, **kwargs)
        request_state = ResponseJobState()
        if not isinstance(jobstate, list):
            jobstate = [jobstate]

        for state in jobstate:
            request_state.request = self.request_builder_class(state)
            request_state.response = request_state.request.execute()
        return request_state


class ResponseJobWrapper(BaseJobWrapper):
    """
    We need this wrapper when we need some post-processing
    after request has been done
    """
    jobstate_class = ResponseJobState


class TestJobWrapper(RequestJobWrapper):

    def __init__(self, *jobs: Callable, initial_jobstate=None):
        from request.builder import TestAPIRequestBuilder

        super(TestJobWrapper, self).__init__(*jobs, initial_jobstate=initial_jobstate)
        self.request_builder_class = TestAPIRequestBuilder


class APIGatewayRequest(RequestJobWrapper):

    def __init__(self, *jobs: Callable, initial_jobstate=None):
        from request.builder import APIGatewayRequestBuilder

        super(APIGatewayRequest, self).__init__(*jobs, initial_jobstate=initial_jobstate)
        self.request_builder_class = APIGatewayRequestBuilder
