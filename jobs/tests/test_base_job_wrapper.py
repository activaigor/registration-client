from unittest import TestCase, mock


class TestBaseJobWrapper(TestCase):

    def test_base_wrapper(self):
        from jobs import BaseJobWrapper

        job1 = mock.MagicMock()
        job_wrapper = BaseJobWrapper(job1)
        job_wrapper()
        job1.assert_called_once()

    def test_base_wrapper_with_initial_state(self):
        from jobs import BaseJobWrapper
        from jobs.states import JobState

        initial_state = JobState()

        job1 = mock.MagicMock()
        job_wrapper = BaseJobWrapper(job1)
        job_wrapper(jobstate=initial_state)
        job1.assert_called_once_with(initial_state)

    def test_base_wrapper_with_wrong_call_state(self):
        from jobs import BaseJobWrapper
        from jobs.states import ResponseJobState, JobState

        call_state = ResponseJobState()
        initial_state = JobState()

        job1 = mock.MagicMock()
        job_wrapper = BaseJobWrapper(job1, initial_jobstate=initial_state)
        job_wrapper(jobstate=call_state)
        self.assertNotEqual(job1.call_args[0][0], call_state)

    def test_base_wrapper_with_correct_call_state(self):
        from jobs import BaseJobWrapper
        from jobs.states import ResponseJobState

        call_state = ResponseJobState()
        initial_state = ResponseJobState()

        job1 = mock.MagicMock()
        job_wrapper = BaseJobWrapper(job1, initial_jobstate=initial_state)
        job_wrapper(jobstate=call_state)
        self.assertEqual(job1.call_args[0][0], call_state)

    @mock.patch('config.DEFAULT_REQUEST_BUILDER')
    def test_request_wrapper(self, req_builder):
        from jobs import RequestJobWrapper
        from jobs.states import JobState

        req_builder_instance = req_builder.return_value
        wrapper_response = mock.MagicMock(spec=JobState)
        req_builder_instance.execute = mock.MagicMock(return_value=wrapper_response)

        job1 = lambda x: x
        job_wrapper = RequestJobWrapper(job1)
        result = job_wrapper()

        req_builder_instance.execute.assert_called_once()
        self.assertIsInstance(result, JobState)

