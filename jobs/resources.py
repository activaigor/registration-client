import os

from jobs.interfaces.resources import Resources
from jobs.states import ServiceStatusState


def get_resources(state: ServiceStatusState) -> ServiceStatusState:
    """
    This task will perform all resource gathering actions to fill up the
    ServiceStatusState. After all necessary fields will be filled this task will throw state
    to the next handler of reducer's chain.
    :param state: ServiceStatusState
    :return: ServiceStatusState
    """

    from blueprint import ServiceBlueprint

    state.cpu_load = Resources.get_cpu_usage()
    state.services = Resources.get_services_status(os.environ)
    state.slug = ServiceBlueprint.data.get('slug')
    state.url = ServiceBlueprint.data.get('url')

    return state

