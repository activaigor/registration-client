from typing import List
import psutil


class Resources:
    """
    All system resources collectors will be implemented here
    """

    @staticmethod
    def get_cpu_usage() -> float:
        return float(psutil.cpu_percent())

    @staticmethod
    def get_services_status(config: dict) -> List[dict]:
        """
        :param config: config (os.environ)
        :return: dict ([{'name': 'mysql', 'status': False}]) and empty dict if no services are configured
        """
        services = Resources._get_services_from_config(config)
        if services:
            return list(map(lambda x: {'name': x['name'],
                                       'status': Resources._check_socket(x['host'], x['port'])}, services))
        else:
            return list()

    @staticmethod
    def _get_services_from_config(config: dict) -> List[dict]:
        """
        :param config: config (os.environ)
        :return: list of dicts ([{'name': 'mysql', 'host': 'localhost', 'port': '123'}])
        """
        from blueprint import ServiceBlueprint
        from blueprint.config.environment import ServiceEnvironConfig
        if not ServiceBlueprint.initialized:
            ServiceBlueprint.initialize(
                ServiceEnvironConfig(config)
            )

        return ServiceBlueprint.data.get('services', [])

    @staticmethod
    def _check_socket(host: str, port: int) -> bool:
        import socket
        try:
            socket.create_connection((host, port), timeout=1)
        except socket.error:
            return False
        else:
            return True

