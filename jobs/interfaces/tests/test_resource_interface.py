from unittest import TestCase, mock

from jobs.interfaces.resources import Resources


class TestResourceInterface(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config_ok = {
            'SERVICE_URL': 'http://localhost:8000/',
            'SERVICE_SLUG': 'slug',
            'SERVICE_NEED_AUTH': False,
            'SERVICE_CLIENT_ID': 'id',
            'SERVICE_CLIENT_SECRET': 'secret',
            'SERVICE_MY_SERVICES': 'mysql://localhost:1234'
        }

    def test_get_services_from_config(self):
        from blueprint import ServiceBlueprint
        ServiceBlueprint.initialized = False
        services = Resources._get_services_from_config(self.config_ok)
        self.assertIsInstance(services, list)
        self.assertEqual(len(services), 1)
        self.assertEqual(services[0]['name'], 'mysql')
        self.assertEqual(services[0]['host'], 'localhost')
        self.assertEqual(services[0]['port'], 1234)

    def test_get_services_status(self):
        with mock.patch.object(Resources, '_get_services_from_config') as services, \
                mock.patch.object(Resources, '_check_socket') as socket:
            socket.return_value = True
            services.return_value = [{'name': 'mysql', 'host': 'localhost', 'port': 1234}]
            result = Resources.get_services_status(self.config_ok)
            self.assertIsInstance(result, list)
            self.assertEqual(len(result), 1)
            self.assertEqual(result[0]['name'], 'mysql')
