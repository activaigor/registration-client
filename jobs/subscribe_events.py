import logging

from typing import List

from jobs import ResponseJobState
from jobs.states import EventSubscribeJobState


def build_subscribe_message(state: ResponseJobState) -> List[EventSubscribeJobState]:
    """
    This handler will use state.request.jobstate (BlueprintJobState) to receive a list of events to subscribe.
    IMPORTANT: Should be executed after registration is finished!
    :param state: ResponseJobState
    :return: None
    """
    if not state.errors and state.response.status_code != 400:
        response_json = state.response.json()
        token = response_json.get('token')
        events = state.request.jobstate.data.get('event_subscribers', [])
        service_slug = state.request.jobstate.data.get('slug')
        states = list()
        for event in events:
            event.update({'service': service_slug})
            new_state = EventSubscribeJobState()
            new_state.data = event
            new_state.token = token
            states.append(new_state)
        return states
    else:
        if state.errors:
            logging.warning('subscribe_events_handler was not executed because of: %s' % state.errors)
        else:
            logging.warning('subscribe_events_handler was not executed because of: %s' % state.response.content)
