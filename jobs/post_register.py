import logging

import os
import redis

from blueprint.config.environment import RedisEnvironConfig
from jobs import ResponseJobState


def post_register_handler(state: ResponseJobState) -> ResponseJobState:
    """
    This handler will log response if register was successfully finished.
    All results from authorization (which expected to be in JSON) will be written into the
    redis database (if connection is available)
    :param state: ResponseJobState
    :return: None
    """
    if not state.errors and state.response.status_code == 200:
        response_json = state.response.json()
        logging.info('Service was successfully authorized: %s' % response_json)

        redis_config = RedisEnvironConfig(os.environ).to_internal_value()
        connection = redis.StrictRedis(**redis_config)
        try:
            connection.ping()
        except redis.ConnectionError:
            logging.warning('Can not connect to redis server (%s). Response will not be saved.' % redis_config)
        else:
            for key, value in response_json.items():
                connection.set(key, value)
            logging.info('Response was successfully saved into redis (%s)' % redis_config)
            return state
    else:
        if state.errors:
            logging.warning('post_register_handler was not executed because of: %s' % state.errors)
        else:
            logging.warning('post_register_handler was not executed because of: %s' % state.response.content)
