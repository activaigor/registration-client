from typing import Callable, ItemsView, List


class JobState:
    """
    Wee need this state to throw it from one job to another.
    JobState will also have a to_request_body method which is very useful
    when we will need to prepare our request body before sending it to API Gateway
    """

    errors = {}

    def error(self, func: Callable, exception: Exception):
        from jobs import BaseJobWrapper

        if isinstance(func, BaseJobWrapper):
            func: BaseJobWrapper
            self.errors[func.__class__.__name__] = str(exception)
        else:
            self.errors[func.__name__] = str(exception)

    def get_errors(self) -> ItemsView:
        return self.errors.items()

    def to_request_body(self) -> dict:
        return self.__dict__


class ResponseJobState(JobState):

    request = None
    response = None


class BlueprintJobState(JobState):

    exclude_fields = ('services',)

    data: dict = None

    def to_request_body(self) -> dict:
        return dict(filter(lambda x: x[0] not in self.exclude_fields, self.data.items()))


class EventSubscribeJobState(ResponseJobState):

    data: dict = None
    token: str = None

    def to_request_body(self) -> dict:
        return self.data


class ServiceStatusState(JobState):

    slug: str = None
    url: str = None
    cpu_load: float = None
    services: List[dict] = None

    def to_request_body(self) -> dict:
        """
        This is useful for preparing data to send it to the API Gateway in the format
        which API Gateway is expected for
        :return: dict
        """
        return dict(
            slug=self.slug,
            url=self.url,
            cpu_load=self.cpu_load,
            services=self.services
        )
