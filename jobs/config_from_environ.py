import os

from blueprint import ServiceBlueprint
from jobs import BlueprintJobState


def config_from_environ(state: BlueprintJobState) -> BlueprintJobState:
    from blueprint.config.environment import ServiceEnvironConfig

    if not ServiceBlueprint.initialized:
        ServiceBlueprint.initialize(
            ServiceEnvironConfig(os.environ)
        )

    state.data = ServiceBlueprint.data
    return state
