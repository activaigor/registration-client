# Registration client

As authorization is necessary part when a new service join the infrastructure, this client should be used as a docker
container like additional layer to handle with SSO authentication requests and responses.

It is designed to have multiple and different config sources but the only one which is currently implemented and 
perfectly suit for usage inside of Docker is a set of environment variables.

You may find this blueprint in bluepring.config.environment.ServiceEnvironConfig which is looks like:
```python
class ServiceEnvironConfig(EnvironConfig):
    """
    This is config for Service Registration blueprint which will be sent
    next to API Gateway where it will be registered and authenticated
    """

    url = fields.URLField(source='SERVICE_URL', required=True)
    slug = fields.StringField(source='SERVICE_SLUG', required=True)
    auth_required = fields.BoolField(source='SERVICE_NEED_AUTH', required=True)
    swagger = fields.SwaggerURLField(source='SERVICE_SWAGGER_URL', required=False)
    client_id = fields.StringField(source='SERVICE_CLIENT_ID', required=True)
    client_secret = fields.StringField(source='SERVICE_CLIENT_SECRET', required=True)
    services = fields.ListField(fields.ServiceField(), source='SERVICE_MY_SERVICES', separator=',', required=False)
    event_subscribers = fields.ListField(fields.EventSubscribeField(), source='EVENT_SUBSCRIBES', separator=',',
                                         required=False, default=[])
```

There are several fields contains the source argument which represents the name of environment variable responsible
on each field. 

Every field describes different feature, which registration client will do after it's startup either by cron (BlockingScheduler)
or execute once when starting.

## Features

### Providing routing information

**Fields**: url, slug, *swagger (not implemented)*

**Description**: fill this field with the url and slug of new service. This data will be used by API Gateway to identify the
name of service (slug) and create a dynamically proxy path for it (/api/{SLUG}/...)

### Authentication

**Fields**: auth_required

**Description**: set auth_required=True if you expect your service to be available only for authenticated requests. This
flag will force API Gateway to validate every request with [SSO service](https://github.com/sentrium/ztelco-sso-server).

### Registration

**Fields**: slug, client_id, client_secret

**Description**: these fields will be sent to the API Gateway right after client startup to register a new service with
provided credentials. These credentials will be verified by SSO. Client will receive JWT token and secret after
registration will be successfully finished.

### Monitoring

**Fields**: services

**Description**: this field is used to provide a simple monitoring feature. Use this field if you expect to use
external services. This field will tell client to provide a simple port-checking feature for them.

**Example**: mysql//localhost:3306,hubspot//api.hubspot.com

### Event subscribing

**Fields**: event_subscribers

**Description**: you may need to describe event_subscribers field if your service depends on 
[one of these events](https://github.com/sentrium/ztelco-api-gateway#known-events). Every event subscription has it's
own structure which consist of name of event and callback URL path (endpoint of current service where data from 
[HTTP dispatcher](https://github.com/sentrium/ztelco-api-gateway#events) will come)

**Example**: eventName:/callback/url,event2:/another/callback/url

