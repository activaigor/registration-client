from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.jobstores.memory import MemoryJobStore
from apscheduler.jobstores.redis import RedisJobStore

from request.builder import RequestBuilder
import os

ENVIRON_MODE_VARIABLE = 'REG_CLIENT_ENV'
ENVIRON_MODE_DEV = 'dev'
ENVIRON_API_GATEWAY_VARIABLE = 'API_GATEWAY'

DEFAULT_API_GATEWAY = 'http://docker.for.mac.localhost:8000/'

SCHEDULER_JOBSTORES = {
    'default': MemoryJobStore()
}
SCHEDULER_EXECUTORS = {
    'default': ThreadPoolExecutor(20)
}

TEST_API_GATEWAY_URL = 'http://docker.for.mac.localhost:8000/'
API_GATEWAY_URL = os.environ.get(ENVIRON_API_GATEWAY_VARIABLE) or DEFAULT_API_GATEWAY

DEFAULT_REQUEST_BUILDER = RequestBuilder

"""
=====================================
    Redis settings
=====================================
"""

DEFAULT_REDIS_DB = 0
DEFAULT_REDIS_HOST = '127.0.0.1'
DEFAULT_REDIS_PORT = 6379

"""
=====================================
    Config settings
=====================================
"""

INTERNAL_SERVICES_FORMAT = r'^(?P<name>.+)\:\/\/(?P<host>.+)\:(?P<port>[0-9]+)$'
EVENT_SUBSCRIBES_FORMAT = r'^(?P<name>.+)\:(?P<callback_url>.+)$'

"""
=====================================
    Scheduler settings
=====================================
"""

SERVICE_UPDATE_INTERVAL = 5

"""
=====================================
    Token headers settings
=====================================
"""

TOKEN_HEADERS_KEY = 'authorization'
TOKEN_HEADERS_PREFIX = 'Bearer'



