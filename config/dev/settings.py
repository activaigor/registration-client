from apscheduler.jobstores.memory import MemoryJobStore
from config.base import *
from request.builder import TestAPIRequestBuilder

SCHEDULER_JOBSTORES = {
    'default': MemoryJobStore()
}

DEFAULT_REQUEST_BUILDER = TestAPIRequestBuilder