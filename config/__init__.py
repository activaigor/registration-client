import os

from config.base import *

if os.environ.get(ENVIRON_MODE_VARIABLE) == ENVIRON_MODE_DEV:
    from config.dev.settings import *
