import os

from apscheduler.events import EVENT_SCHEDULER_START
from apscheduler.schedulers.blocking import BlockingScheduler

from blueprint import ServiceBlueprint
from blueprint.config.environment import ServiceEnvironConfig
from config import SCHEDULER_JOBSTORES, SCHEDULER_EXECUTORS, SERVICE_UPDATE_INTERVAL
from config.logger import prepare_logger
from jobs.config_from_environ import config_from_environ
from jobs.post_register import post_register_handler
from jobs.subscribe_events import build_subscribe_message

if __name__ == '__main__':
    prepare_logger()

    ServiceBlueprint.initialize(
        ServiceEnvironConfig(os.environ)
    )

    scheduler = BlockingScheduler(jobstores=SCHEDULER_JOBSTORES, executors=SCHEDULER_EXECUTORS)

    from jobs import APIGatewayRequest, BlueprintJobState, ResponseJobWrapper, TestJobWrapper
    from jobs.states import ResponseJobState, EventSubscribeJobState

    scheduler.add_listener(
        ResponseJobWrapper(
            APIGatewayRequest(config_from_environ, initial_jobstate=BlueprintJobState()),
            post_register_handler,
            APIGatewayRequest(build_subscribe_message, initial_jobstate=ResponseJobState())
        ),
        EVENT_SCHEDULER_START
    )

    from jobs.states import ServiceStatusState
    from jobs.resources import get_resources

    scheduler.add_job(
        APIGatewayRequest(
            get_resources,
            initial_jobstate=ServiceStatusState()
        ),
        'interval',
        seconds=SERVICE_UPDATE_INTERVAL
    )

    scheduler.start()
