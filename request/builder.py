import json
import logging
from functools import partial
from urllib.parse import urlencode, urljoin

import requests


class RequestBuilder:

    url: str = None
    method: str = None

    from jobs import JobState

    def __init__(self, jobstate: JobState):
        self.jobstate = jobstate

    def execute(self):
        if not self.url:
            logging.error("There are no URL for %s, so request can't be send" % self.__class__.__name__)
            return

        request_function = self._get_request_function()
        return request_function()

    @staticmethod
    def _get_request_data(url: str, method: str, jobstate: JobState) -> (str, str, dict):
        """
        Prepare url, method and request_body for building correct request
        :param url: url
        :param method: method
        :param jobstate: JobState instance
        :return: url, method, request_body
        """
        request_body = jobstate.to_request_body()
        return url, method, request_body

    @staticmethod
    def _get_request_headers(jobstate: JobState) -> dict:
        """
        Prepare headers for request, based on current jobstate
        :param jobstate: JobState instance
        :return: dict of headers
        """
        return {'content-type': 'application/json'}

    def _get_request_function(self) -> partial:
        url, method, request_body = self._get_request_data(self.url, self.method, self.jobstate)
        headers = self._get_request_headers(self.jobstate)

        if method == 'get':
            req_func = partial(requests.get, '%s?%s' % (url, urlencode(request_body)),
                               headers=headers)
        else:
            req_func = partial(getattr(requests, method), url, data=json.dumps(request_body),
                               headers=headers)
        return req_func


class TestAPIRequestBuilder(RequestBuilder):

    method = 'get'

    def __init__(self, jobstate):
        from config import TEST_API_GATEWAY_URL

        super(TestAPIRequestBuilder, self).__init__(jobstate)
        self.url = TEST_API_GATEWAY_URL


class APIGatewayRequestBuilder(RequestBuilder):
    """
    This request builder is responsible to prepare all necessary data
    before request to the API Gateway will be sent.
    APIGatewayRequestBuilder make his decisions based on jobstate and has the next strategies:
    - POST to the URL_REGISTER_SERVICE if it is BlueprintJobState
    """

    URL_REGISTER_SERVICE = 'services/registration/'
    URL_SERVICE_UPDATE = 'services/{slug}/'
    URL_SUBSCRIBE_EVENT = 'events/subscriptions/'

    def __init__(self, jobstate):
        from config import API_GATEWAY_URL

        super(APIGatewayRequestBuilder, self).__init__(jobstate)
        self.url = API_GATEWAY_URL

    from jobs import JobState

    @staticmethod
    def _get_request_data(url: str, method: str, jobstate: JobState) -> (str, str, dict):
        request_body = jobstate.to_request_body()

        from jobs import BlueprintJobState
        from jobs.states import ServiceStatusState, EventSubscribeJobState

        if isinstance(jobstate, BlueprintJobState):
            method = 'post'
            url = urljoin(url, APIGatewayRequestBuilder.URL_REGISTER_SERVICE)
        elif isinstance(jobstate, EventSubscribeJobState):
            method = 'post'
            url = urljoin(url, APIGatewayRequestBuilder.URL_SUBSCRIBE_EVENT)
        elif isinstance(jobstate, ServiceStatusState):
            method = 'put'
            url = urljoin(url, APIGatewayRequestBuilder.URL_SERVICE_UPDATE.format(slug=jobstate.slug))

        return url, method, request_body

    @staticmethod
    def _get_request_headers(jobstate: JobState) -> dict:
        from jobs.states import EventSubscribeJobState
        from config import TOKEN_HEADERS_KEY, TOKEN_HEADERS_PREFIX

        if isinstance(jobstate, EventSubscribeJobState):
            return {
                TOKEN_HEADERS_KEY: f'{TOKEN_HEADERS_PREFIX} {jobstate.token}',
                'content-type': 'application/json'
            }
        else:
            return {'content-type': 'application/json'}





