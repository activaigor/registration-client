from unittest import TestCase, mock
from urllib.parse import urlencode
from request.builder import RequestBuilder


class RequestBuilderTest(RequestBuilder):
    url = 'http://google.com/'
    method = 'get'


class TestRequestBuilder(TestCase):

    @mock.patch('requests.get')
    def test_request_builder(self, mock_requests):
        jobstate_req_body = {'key': 'value'}
        jobstate = mock.MagicMock()
        jobstate.to_request_body = mock.MagicMock(return_value=jobstate_req_body)

        request_builder = RequestBuilderTest(jobstate)
        request_builder.execute()

        jobstate.to_request_body.assert_called_once()
        expected_url = '%s?%s' % (RequestBuilderTest.url, urlencode(jobstate_req_body))
        mock_requests.assert_called_once_with(expected_url)

