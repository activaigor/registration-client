from blueprint.config import BlueprintConfig


class ServiceBlueprint:

    initialized: bool = False
    data: dict = None

    @classmethod
    def initialize(cls, conf: BlueprintConfig):
        cls.data = conf.to_internal_value()
        cls.initialized = True

