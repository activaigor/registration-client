from unittest import TestCase

from blueprint import BlueprintConfig, fields
from blueprint.fields import FieldValidationError


class BlueprintEventConfigTest(BlueprintConfig):

    event_subscribers = fields.ListField(fields.EventSubscribeField(), source='EVENT_SUBSCRIBES', separator=',',
                                         required=False)

    def _parse_source(self) -> dict:
        return dict(self.source)


class BlueprintConfigTest(BlueprintConfig):

    url = fields.URLField(source='URL', required=True)
    slug = fields.StringField(source='SLUG', required=True)
    auth_required = fields.BoolField(source='AUTH', required=False, default=True)

    def _parse_source(self) -> dict:
        return dict(self.source)


class TestConfig(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_blueprint_cls = BlueprintConfigTest
        cls.test_blueprint_event_cls = BlueprintEventConfigTest
        cls.data_ok = {
            'URL': 'http://google.com/',
            'SLUG': 'slug',
            'AUTH': True
        }
        cls.data_url_invalid = {
            'URL': 'invalid_url',
            'SLUG': 'slug',
            'AUTH': True
        }
        cls.data_required_error = {
            'URL': 'http://google.com/',
            'AUTH': True
        }
        cls.event_data_ok = {
            'EVENT_SUBSCRIBES': 'myEvent:/super/callback?param=123'
        }

    def setUp(self):
        pass

    def test_get_config_fields(self):
        from blueprint.fields import BaseField
        bluepring = self.test_blueprint_cls(self.data_ok)
        fields = bluepring._get_config_fields()
        self.assertIsInstance(fields, map)
        fields_dict = dict(fields)
        self.assertEqual(len(fields_dict.keys()), 3)
        self.assertTrue(all([isinstance(x, BaseField) for x in fields_dict.values()]))

    def test_event_config_field(self):
        bluepring = self.test_blueprint_event_cls(self.event_data_ok)
        fields = bluepring.to_internal_value()
        self.assertIsInstance(fields['event_subscribers'], list)
        self.assertEqual(len(fields['event_subscribers']), 1)
        self.assertEqual(fields['event_subscribers'][0]['name'], 'myEvent')
        self.assertEqual(fields['event_subscribers'][0]['callback_url'], '/super/callback?param=123')

    def test_to_internal_value_ok(self):
        bluepring = self.test_blueprint_cls(self.data_ok)
        result = bluepring.to_internal_value()
        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)
        self.assertNotEqual(result.keys(), self.data_ok.keys())
        expected_keys_for_internal_value = ('url', 'slug', 'auth_required')
        self.assertTrue(all([x in expected_keys_for_internal_value for x in result.keys()]))

    def test_to_internal_value_error_on_url(self):
        from blueprint.config import ConfigException

        bluepring = self.test_blueprint_cls(self.data_url_invalid)
        with self.assertRaises(ConfigException) as exc:
            result = bluepring.to_internal_value()
            self.assertIsNone(result)

        self.assertIsInstance(exc.exception.args[0], dict)
        self.assertIsInstance(exc.exception.args[0]['details'], dict)
        self.assertIn('url', exc.exception.args[0]['details'].keys())

    def test_to_internal_value_error_on_required(self):
        from blueprint.config import ConfigException

        bluepring = self.test_blueprint_cls(self.data_required_error)
        with self.assertRaises(ConfigException) as exc:
            result = bluepring.to_internal_value()
            self.assertIsNone(result)

        self.assertIsInstance(exc.exception.args[0], dict)
        self.assertIsInstance(exc.exception.args[0]['details'], dict)
        self.assertIn('slug', exc.exception.args[0]['details'].keys())
        self.assertEqual(exc.exception.args[0]['details']['slug'], 'This field is required!')