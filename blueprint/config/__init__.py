from typing import Iterator, Tuple
from blueprint.fields import BaseField, FieldValidationError


class ConfigException(Exception):
    pass


class BlueprintConfig:

    exception_text = 'There are some errors in your config.'

    def __init__(self, source):
        self.source = source

    def to_internal_value(self) -> dict:
        errors = dict()
        exceptions = list()
        result = dict()
        data = self._parse_source()
        for field_name, field in self._get_config_fields():
            try:
                field.to_internal_value(data.get(field.source))
                result[field_name] = field.data
            except FieldValidationError as exc:
                errors[field_name] = str(exc)
                exceptions.append(exc)

        if errors:
            raise ConfigException({'message': self.exception_text, 'details': errors}) from exceptions[-1]
        return result

    def _parse_source(self):
        raise NotImplementedError

    def _get_config_fields(self) -> Iterator[Tuple[str, BaseField]]:
        return map(lambda f: (f, getattr(self, f)),
                   filter(lambda y: isinstance(getattr(self, y, None), BaseField),
                      filter(lambda x: not x.startswith('__'), dir(self))))
