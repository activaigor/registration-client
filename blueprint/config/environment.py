from blueprint.config import BlueprintConfig
from blueprint import fields
from config import DEFAULT_REDIS_HOST, DEFAULT_REDIS_PORT, DEFAULT_REDIS_DB


class EnvironConfig(BlueprintConfig):
    """
    This config is adapted to work with os.environ as a source of data
    """
    exception_text = 'There are some errors in your environment.'

    def _parse_source(self) -> dict:
        return dict(self.source)


class ServiceEnvironConfig(EnvironConfig):
    """
    This is config for Service Registration blueprint which will be sent
    next to API Gateway where it will be registered and authenticated
    """

    url = fields.URLField(source='SERVICE_URL', required=True)
    slug = fields.StringField(source='SERVICE_SLUG', required=True)
    auth_required = fields.BoolField(source='SERVICE_NEED_AUTH', required=True)
    swagger = fields.SwaggerURLField(source='SERVICE_SWAGGER_URL', required=False)
    client_id = fields.StringField(source='SERVICE_CLIENT_ID', required=True)
    client_secret = fields.StringField(source='SERVICE_CLIENT_SECRET', required=True)
    services = fields.ListField(fields.ServiceField(), source='SERVICE_MY_SERVICES', separator=',', required=False)
    event_subscribers = fields.ListField(fields.EventSubscribeField(), source='EVENT_SUBSCRIBES', separator=',',
                                         required=False, default=[])


class RedisEnvironConfig(EnvironConfig):
    """
    This is config for Service Registration blueprint which will be sent
    next to API Gateway where it will be registered and authenticated
    """

    host = fields.StringField(source='REDIS_HOST', default=DEFAULT_REDIS_HOST, required=False)
    port = fields.IntField(source='REDIS_PORT', default=DEFAULT_REDIS_PORT, required=False)
    db = fields.IntField(source='REDIS_DATABASE', default=DEFAULT_REDIS_DB, required=False)


