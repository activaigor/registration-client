from unittest import TestCase, mock


class TestBlueprint(TestCase):

    def test_blueprint_initialize(self):
        from blueprint import BlueprintConfig
        from blueprint import ServiceBlueprint
        test_data = {'data': 'test'}

        bluepring_config = mock.MagicMock(spec=BlueprintConfig)
        bluepring_config_instance = bluepring_config(test_data)
        bluepring_config_instance.to_internal_value.return_value = test_data

        ServiceBlueprint.initialize(bluepring_config_instance)

        bluepring_config_instance.to_internal_value.assert_called_once()
        self.assertEqual(ServiceBlueprint.data, test_data)


