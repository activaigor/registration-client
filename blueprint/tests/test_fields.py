from unittest import TestCase

from blueprint.fields import ListField, StringField, ServiceField


class TestFields(TestCase):

    def test_list_field(self):
        list_field = ListField(StringField())
        list_field.to_internal_value([1, 2, 3])
        self.assertListEqual(list_field.data, ['1', '2', '3'])

    def test_list_field_with_separator(self):
        list_field = ListField(StringField(), separator=',')
        list_field.to_internal_value('1, 2, 3')
        self.assertListEqual(list_field.data, ['1', '2', '3'])

    def test_service_field(self):
        service_field = ServiceField()
        service_field.to_internal_value('mysql://localhost:123')
        self.assertIsInstance(service_field.data, dict)
        self.assertEqual(service_field.data['name'], 'mysql')
        self.assertEqual(service_field.data['host'], 'localhost')
        self.assertIsInstance(service_field.data['port'], int)
        self.assertEqual(service_field.data['port'], 123)

    def test_service_field_wrong_format(self):
        service_field = ServiceField()
        service_field.to_internal_value('wrong-format')
        self.assertIsNone(service_field.data)

