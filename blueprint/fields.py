import re


class FieldValidationError(Exception):
    pass


class BaseField:

    def __init__(self, source: str=None, default=None, required=False):
        self._data = None
        self.source = source
        self.default = default
        self.required = required

    def to_internal_value(self, value) -> None:
        self.data = self._to_internal_value(value)

    @staticmethod
    def _to_internal_value(value):
        return value

    @property
    def data(self):
        if self._data is None:
            return self.default
        else:
            return self._data

    @data.setter
    def data(self, value):
        if self.required and value is None:
            self._data = None
            raise FieldValidationError("This field is required!")
        else:
            self._data = value


class ListField(BaseField):
    """
    This field should be provided with child fields. It may be also declared with separator,
    what can be very useful when use strings separated by some symbol
    """

    def __init__(self, child_field: BaseField, source: str=None, default=None, required=False, separator=None):
        self.child_field = child_field
        self.separator = separator
        super(ListField, self).__init__(source=source, default=default, required=required)

    def _to_internal_value(self, value) -> list or None:
        try:
            if self.separator:
                value = value.replace(' ', '').split(self.separator)
            if isinstance(value, list):
                result = list()
                for field in value:
                    self.child_field.to_internal_value(field)
                    result.append(self.child_field.data)
                return result
            else:
                return None
        except Exception:
            return None


class StringField(BaseField):

    @staticmethod
    def _to_internal_value(value) -> str or None:
        try:
            return str(value) if value is not None else value
        except Exception:
            return None


class ServiceField(StringField):

    @staticmethod
    def _to_internal_value(value: str) -> dict or None:
        """
        :param value: str (mysql//localhost:123)
        :return: dict ({'name': 'mysql', 'host': 'localhost', 'port': '123'})
        """
        value = StringField._to_internal_value(value)
        if value is not None:
            return ServiceField._parse_service_uri(value)
        else:
            return None

    @staticmethod
    def _parse_service_uri(value: str) -> dict or None:
        from config import INTERNAL_SERVICES_FORMAT
        try:
            match = re.match(INTERNAL_SERVICES_FORMAT, value)
            dict_match = match.groupdict()
            dict_match['port'] = int(dict_match['port'])
            return dict_match
        except (AttributeError, ValueError):
            return None


class EventSubscribeField(StringField):

    @staticmethod
    def _to_internal_value(value: str) -> dict or None:
        """
        :param value: str (eventName:/callback/url)
        :return: dict ({'name': 'eventName', 'callback_url': '/callback/url'})
        """
        value = StringField._to_internal_value(value)
        if value is not None:
            return EventSubscribeField._parse_service_uri(value)
        else:
            return None

    @staticmethod
    def _parse_service_uri(value: str) -> dict or None:
        from config import EVENT_SUBSCRIBES_FORMAT
        try:
            match = re.match(EVENT_SUBSCRIBES_FORMAT, value)
            return match.groupdict()
        except (AttributeError, ValueError):
            return None


class BoolField(BaseField):

    @staticmethod
    def _to_internal_value(value) -> bool:
        if value in ('1', '0'):
            value = int(value)
        elif value in ('True', 'False'):
            value = True if value == 'True' else False
        return bool(value)


class IntField(BaseField):

    @staticmethod
    def _to_internal_value(value) -> int or None:
        try:
            return int(value)
        except Exception:
            return None


class URLField(BaseField):

    @staticmethod
    def _to_internal_value(value) -> str or None:
        if URLField._valid_url(value):
            return value
        else:
            raise FieldValidationError('Url %s is not valid' % value)

    @staticmethod
    def _valid_url(url):
        from urllib.parse import urlparse
        try:
            result = urlparse(url)
            return all([result.scheme, result.netloc, result.path])
        except Exception:
            return False


class SwaggerURLField(URLField):
    """
    This field will fetch the url, trying to get json OpenAPI schema
    with provided url. It also checks if url is valid
    """

    @staticmethod
    def _to_internal_value(value) -> dict or None:
        import requests
        if SwaggerURLField._valid_url(value):
            result = requests.get(value)
            if result.status_code in (200, 304):
                return result.json()
        else:
            return None
